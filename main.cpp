#include "triangulo.cpp"
#include "quadrado.cpp"
#include "geometrica.cpp"
#include <iostream>

using namespace std;

int main(){

	Triangulo *x = new Triangulo(50, 50);
	Quadrado *y = new Quadrado (50,50);
	cout << "Area do quadrado = "<< y->area() << endl; 
	cout << "Area do triangulo = "<< x->area() << endl; 
	
	return 0;
}