#include "quadrado.hpp"

using namespace std;

Quadrado::Quadrado(){
	setBase(10);
	setAltura(10);
}

Quadrado::Quadrado(float altura, float base){
	setBase(base);
	setAltura(altura);
}

float Quadrado::area(){
	return getAltura() * getBase();
}

float Quadrado::area(float altura, float base){
	return altura * base;
}

