#ifndef QUADRADO_H
#define QUADRADO_H

#include "geometrica.hpp"

class Quadrado : public Geometrica{
	public:
		Quadrado();
		Quadrado(float altura, float base);
		float area();
		float area(float altura, float base);
};

#endif