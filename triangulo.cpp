#include "triangulo.hpp"

using namespace std;

Triangulo::Triangulo(){
	setBase(10);
	setAltura(10);
}

Triangulo::Triangulo(float altura, float base){
	setBase(base);
	setAltura(altura);
}

float Triangulo::area(){
	return getAltura() * getBase() / 2;
}

float Triangulo::area(float altura, float base){
	return altura * base / 2;
}